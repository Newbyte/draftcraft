/*
 * Copyright (C) Newbyte. All rights reserved.
 *
 * This file is part of Draftcraft, distributed under the zlib licence. For
 * full terms see the included LICENCE file.
 */

#version 330 core

in vec2 pass_textureCoords;

uniform sampler2D textureSampler;

out vec4 out_Color;

void main(void) {
    out_Color = texture(textureSampler, pass_textureCoords);
}
