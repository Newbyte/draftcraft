# Draftcraft

This is a voxel "game" I made for a Java course at university. There isn't much
in the way of gameplay implemented, but you can walk around and place and
remove blocks. There's something wrong with the raytracing I implemented, so it
is hard to aim. That said, it is technically possible to place and remove
blocks.

Draftcraft uses Maven for dependency management, and depends on various LWJGL
modules.