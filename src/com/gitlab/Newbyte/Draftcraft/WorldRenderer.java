/*
 * Copyright (C) Newbyte. All rights reserved.
 *
 * This file is part of Draftcraft, distributed under the zlib licence. For
 * full terms see the included LICENCE file.
 */

package com.gitlab.Newbyte.Draftcraft;

import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

import static com.gitlab.Newbyte.Draftcraft.Constants.*;

/**
 * Manages rendering the blocks that make up the game world.
 */
public class WorldRenderer {
    private final ChunkManager chunkManager;
    private final Maths maths;
    private final StaticShader shader;
    private final TexturedModel sandBlockModel;
    private final TexturedModel stoneBlockModel;

    public WorldRenderer(
            final ChunkManager chunkManager,
            final Maths maths,
            final StaticShader shader,
            final TexturedModel sandBlockModel,
            final TexturedModel stoneBlockModel
    ) {
        this.chunkManager = chunkManager;
        this.maths = maths;
        this.shader = shader;
        this.sandBlockModel = sandBlockModel;
        this.stoneBlockModel = stoneBlockModel;
    }

    /**
     * Renders all the blocks in the world.
     */
    public void renderBlocks() {
        final BlockType[][][] blocks = chunkManager.getBlocks();

        for (int x = 0; x < blocks.length; x++) {
            for (int y = 0; y < blocks[x].length; y++) {
                for (int z = 0; z < blocks[x][y].length; z++) {
                    final BlockType currentBlock = blocks[x][y][z];
                    if (currentBlock != null) {
                        final TexturedModel modelToRender;

                        if (currentBlock == BlockType.SAND) {
                            modelToRender = sandBlockModel;
                        } else {
                            modelToRender = stoneBlockModel;
                        }

                        renderBlock(modelToRender, new Vector3f(x, y, z), shader);
                    }
                }
            }
        }
    }

    /**
     * Renders a single block.
     * @param blockModel The model to render as a block.
     * @param position The position to render the block at.
     * @param shader The StaticShader instance to use for rendering.
     */
    public void renderBlock(final TexturedModel blockModel, final Vector3f position, final StaticShader shader) {
        GL30.glBindVertexArray(blockModel.getVaoName());
        GL20.glEnableVertexAttribArray(0);
        GL20.glEnableVertexAttribArray(1);

        final Matrix4f transformationMatrix =
                maths.makeTransformationMatrix(position, new Vector3f(0, 0, 0), 1);
        shader.loadTransformationMatrix(transformationMatrix);

        GL13.glActiveTexture(GL13.GL_TEXTURE0);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, blockModel.getTextureName());
        GL11.glDrawElements(GL11.GL_TRIANGLES, blockModel.getVertexCount(), GL11.GL_UNSIGNED_INT, 0);
        GL20.glDisableVertexAttribArray(0);
        GL20.glDisableVertexAttribArray(1);
        GL30.glBindVertexArray(NONE);
    }
}
