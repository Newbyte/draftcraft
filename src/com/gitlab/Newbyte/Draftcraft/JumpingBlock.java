/*
 * Copyright (C) Newbyte. All rights reserved.
 *
 * This file is part of Draftcraft, distributed under the zlib licence. For
 * full terms see the included LICENCE file.
 */

package com.gitlab.Newbyte.Draftcraft;

import org.joml.Vector3f;

/**
 * An example entity that can be spawned in the world that stays in place and jumps.
 */
public class JumpingBlock extends Entity {
    private final TexturedModel texturedModel;
    private final WorldRenderer worldRenderer;
    private final StaticShader staticShader;

    protected JumpingBlock(
            final Vector3f position,
            final ChunkManager chunkManager,
            final TexturedModel texturedModel,
            final WorldRenderer worldRenderer,
            final StaticShader staticShader
            ) {
        super(position, chunkManager);
        this.texturedModel = texturedModel;
        this.worldRenderer = worldRenderer;
        this.staticShader = staticShader;
    }

    @Override
    public void onTick() {
        super.onTick();
        jump();
    }

    @Override
    protected int getHeight() {
        return 1;
    }

    @Override
    public void render() {
        worldRenderer.renderBlock(texturedModel, position, staticShader);
    }
}
