/*
 * Copyright (C) Newbyte. All rights reserved.
 *
 * This file is part of Draftcraft, distributed under the zlib licence. For
 * full terms see the included LICENCE file.
 */

package com.gitlab.Newbyte.Draftcraft;

/**
 * Interface for listening for mouse input; mouse button presses and cursor movement.
 */
public interface InputListener {
    /**
     * Called when a mouse button is pressed.
     * @param button The button that was pressed. The value comes from GLFW.
     */
    void mouseButtonPressed(final int button);

    /**
     * Called when the mouse is moved.
     * @param deltaX The delta mouse movement in the X axis since the last call.
     * @param deltaY The delta mouse movement in the Y axis since the last call.
     */
    void cursorMoved(final double deltaX, final double deltaY);
}
