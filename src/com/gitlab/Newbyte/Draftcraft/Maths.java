/*
 * Copyright (C) Newbyte. All rights reserved.
 *
 * This file is part of Draftcraft, distributed under the zlib licence. For
 * full terms see the included LICENCE file.
 */

package com.gitlab.Newbyte.Draftcraft;

import org.joml.Matrix4f;
import org.joml.Vector3f;

import static com.gitlab.Newbyte.Draftcraft.RenderManager.FAR_PLANE;
import static com.gitlab.Newbyte.Draftcraft.RenderManager.FOV;
import static com.gitlab.Newbyte.Draftcraft.RenderManager.NEAR_PLANE;

public class Maths {
    /**
     * Creates a transformation matrix.
     * @param translation The translation to translate the matrix by.
     * @param rotation The rotation to rotate the matrix by.
     * @param scale The scale to scale the matrix by.
     * @return A transformation matrix.
     */
    public Matrix4f makeTransformationMatrix(
            final Vector3f translation,
            final Vector3f rotation,
            final float scale
    ) {
        return new Matrix4f()
                .identity()
                .translate(translation)
                .rotateX(rotation.x)
                .rotateY(rotation.y)
                .rotateZ(rotation.z)
                .scale(scale);
    }

    /**
     * Creates a projection matrix.
     * @param displayWidth The width of the window displaying the game.
     * @param displayHeight The height of the window displaying the game.
     * @return A projection matrix.
     */
    public Matrix4f makeProjectionMatrix(final int displayWidth, final int displayHeight) {
        final float aspectRatio = (float) displayWidth / (float) displayHeight;
        final float yScale = (float) (1f / Math.tan(Math.toRadians(FOV / 2f)));
        final float xScale = yScale / aspectRatio;
        final float zp = FAR_PLANE + NEAR_PLANE;
        final float zm = FAR_PLANE - NEAR_PLANE;

        return new Matrix4f()
                .m00(xScale)
                .m11(yScale)
                .m22(-zp / zm)
                .m23(-1)
                .m32(-(2 * FAR_PLANE * NEAR_PLANE) / zm)
                .m33(0);
    }

    /**
     * Creates a view matrix.
     * @param camera The camera to create the view matrix' perspective from.
     * @return A view matrix.
     */
    public Matrix4f makeViewMatrix(final Camera camera) {
        final Vector3f cameraPosition = camera.getPosition();
        final FirstPersonOrientation orientation = camera.getOrientation();

        return new Matrix4f()
                .identity()
                .rotateX(orientation.pitch)
                .rotateZ(orientation.yaw)
                .translate(-cameraPosition.x, -cameraPosition.y, -cameraPosition.z);
    }
}
