/*
 * Copyright (C) Newbyte. All rights reserved.
 *
 * This file is part of Draftcraft, distributed under the zlib licence. For
 * full terms see the included LICENCE file.
 */

package com.gitlab.Newbyte.Draftcraft;

import org.joml.Vector2d;
import org.lwjgl.glfw.*;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.system.MemoryUtil;

import java.nio.IntBuffer;
import java.util.*;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.system.MemoryUtil.NULL;

/**
 * Class for handling GLFW (window and input management) in a more Java-idiomatic way.
 */
public class WindowManager {
    private final static int DEFAULT_WIDTH = 1280;
    private final static int DEFAULT_HEIGHT = 720;
    private final Logger logger = Logger.getLogger(WindowManager.class.getName());
    private final Set<Integer> pressedKeys = new HashSet<>();
    private final Set<Integer> pressedKeysView = Collections.unmodifiableSet(pressedKeys);
    private final List<InputListener> inputListeners = new ArrayList<>();
    private final Vector2d cursor = new Vector2d();
    private final long window;

    /**
     * Constructs a new WindowManager.
     * @param logFileHandler A FileHandler pointing to the game's log file.
     * @throws IllegalStateException Thrown if GLFW fails to initialise.
     */
    public WindowManager(final FileHandler logFileHandler) throws IllegalStateException {
        if (logFileHandler != null) {
            logger.addHandler(logFileHandler);
        }

        glfwSetErrorCallback(new GLFWErrorCallback() {
            @Override
            public void invoke(int error, long descriptionPointer) {
                final String errorDescription = MemoryUtil.memUTF8(descriptionPointer);
                logger.log(Level.SEVERE, "GLFW reported an error with code " + error  + ": " + errorDescription);
            }
        });

        if (!glfwInit()) {
            throw new IllegalStateException("Failed to initialise GLFW!");
        }

        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
        glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

        // TODO: Check with system that these are reasonable dimensions?
        window = glfwCreateWindow(DEFAULT_WIDTH, DEFAULT_HEIGHT, Constants.APP_NAME, NULL, NULL);

        // I'm not catching here because I don't think memory allocation failures should be handled.
        try (final MemoryStack stack = MemoryStack.stackPush()) {
            final IntBuffer pWidth = stack.mallocInt(1);
            final IntBuffer pHeight = stack.mallocInt(1);


            glfwGetWindowSize(window, pWidth, pHeight);
            final GLFWVidMode videoMode = glfwGetVideoMode(glfwGetPrimaryMonitor());

            assert videoMode != null;
            glfwSetWindowPos(
                    window,
                    (videoMode.width() - pWidth.get(0)) / 2,
                    (videoMode.height() - pHeight.get(0)) / 2
            );
        }

        glfwMakeContextCurrent(window);
        GL.createCapabilities();
        GL11.glViewport(0, 0, DEFAULT_WIDTH, DEFAULT_HEIGHT);
        glfwSwapInterval(1);
        glfwShowWindow(window);

        glfwSetKeyCallback(window, new GLFWKeyCallback() {
            @Override
            public void invoke(final long window, final int key, final int scancode, final int action, final int mods) {
                switch (action) {
                    case GLFW_PRESS -> {
                        pressedKeys.add(key);

                        switch (key) {
                            case GLFW_KEY_ESCAPE -> glfwSetWindowShouldClose(window, true);
                            case GLFW_KEY_F6 -> toggleCursorMode();
                        }
                    }
                    case GLFW_RELEASE -> pressedKeys.remove(key);
                }
            }
        });

        glfwSetMouseButtonCallback(window, new GLFWMouseButtonCallback() {
            @Override
            public void invoke(final long window, final int button, final int action, final int mods) {
                notifyMouseButtonPressed(button);
            }
        });

        glfwSetCursorPosCallback(window, new GLFWCursorPosCallback() {
            @Override
            public void invoke(final long window, final double positionX, final double positionY) {
                final double deltaX = positionX - cursor.x;
                final double deltaY = positionY - cursor.y;

                cursor.x = positionX;
                cursor.y = positionY;

                notifyCursorMoved(deltaX, deltaY);
            }
        });
    }

    /**
     * Returns the default window width.
     * @return The default window width.
     */
    public int getDefaultWidth() {
        return DEFAULT_WIDTH;
    }

    /**
     * Returns the default window height.
     * @return The default window height.
     */
    public int getDefaultHeight() {
        return DEFAULT_HEIGHT;
    }

    /**
     * Returns an immutable view of all currently pressed keys.
     * @return An immutable view of all currently pressed keys.
     */
    public Set<Integer> getPressedKeysView() {
        return pressedKeysView;
    }

    /**
     * Subscribe the object passed as listener to input events.
     * @param listener Callback to be called when input happens.
     */
    public void addInputListener(final InputListener listener) {
        inputListeners.add(listener);
    }

    /**
     * Polls for input events and swaps buffers so the display gets updated.
     */
    public void updateDisplay() {
        glfwPollEvents();
        glfwSwapBuffers(window);
    }

    /**
     * Toggles between the cursor being captured by the game window and free to move.
     */
    public void toggleCursorMode() throws IllegalStateException {
       final int currentCursorMode = glfwGetInputMode(window, GLFW_CURSOR);

       glfwSetInputMode(
               window,
               GLFW_CURSOR,
               currentCursorMode == GLFW_CURSOR_DISABLED ? GLFW_CURSOR_NORMAL : GLFW_CURSOR_DISABLED
       );
    }

    /**
     * Destroys the internally tracked window. This should only be used to free up resources, not close the window.
     */
    public void destroy() {
        glfwDestroyWindow(window);
    }

    /**
     * Wrapper for glfwWindowShouldClose from GLFW.
     * @return Whether the window should close.
     */
    public boolean shouldWindowClose() {
        return glfwWindowShouldClose(window);
    }

    /**
     * Notifies all listeners that the provided mouse button has been pressed.
     * @param button The index of the button that was pressed. Provided by GLFW.
     */
    private void notifyMouseButtonPressed(final int button) {
        inputListeners.forEach((inputListener -> inputListener.mouseButtonPressed(button)));
    }

    /**
     * Notifies all listeners that the cursor was moved.
     * @param deltaX The delta mouse movement in the X axis since the last call.
     * @param deltaY The delta mouse movement in the Y axis since the last call.
     */
    private void notifyCursorMoved(final double deltaX, final double deltaY) {
        inputListeners.forEach((listener -> listener.cursorMoved(deltaX, deltaY)));
    }
}
