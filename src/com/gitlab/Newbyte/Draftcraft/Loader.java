/*
 * Copyright (C) Newbyte. All rights reserved.
 *
 * This file is part of Draftcraft, distributed under the zlib licence. For
 * full terms see the included LICENCE file.
 */

package com.gitlab.Newbyte.Draftcraft;

import org.lwjgl.opengl.*;
import org.lwjgl.BufferUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import static com.gitlab.Newbyte.Draftcraft.Constants.NONE;

/**
 * Utility class for loading various things into GPU memory.
 */
public class Loader {
    private final static String TEXTURE_PATH = "textures/";
    private final static int BYTES_PER_PIXEL = 4;
    private final List<Integer> vaoList = new ArrayList<>();
    private final List<Integer> vboList = new ArrayList<>();
    private final List<Integer> textureList = new ArrayList<>();

    /**
     * Loads a list of vertex coordinates, indexes, and uv coordinates.
     * Rather than explaining these here I suggest you read about vertex buffers, index buffers, and UV mapping online
     * as these are rather universal concepts in 3D graphics and I can't hope to cover them as well as other people
     * already have.
     * @param vertices List of vertices to compose the model from.
     * @param indices Index buffer.
     * @param uv UV mapping.
     * @return A BareModel made from the provided data.
     */
    public int loadToVao(final FloatBuffer vertices, final IntBuffer indices, final FloatBuffer uv) {
        final int vaoName = GL30.glGenVertexArrays();
        vaoList.add(vaoName);
        GL30.glBindVertexArray(vaoName);
        storeDataInAttributeList(vertices, 0, 3);
        storeDataInAttributeList(uv, 1, 2);
        bindIndicesBuffer(indices);
        GL30.glBindVertexArray(NONE);
        
        return vaoName;
    }

    /**
     * Uploads a texture to the GPU and returns the name given by OpenGL. Also adds it to a list of generated texture
     * names that are cleaned on exit.
     * Taken from https://stackoverflow.com/questions/10801016/lwjgl-textures-and-strings/10872080#10872080
     * @param filepath Path to the texture to load.
     * @return The texture name assigned to the loaded texture by OpenGL.
     * @throws IOException Thrown when the texture could not be loaded from disk.
     */
    public int loadTexture(final String filepath) throws IOException {
        final InputStream fileStream = ClassLoader.getSystemResourceAsStream(TEXTURE_PATH + filepath);
        assert fileStream != null;
        final BufferedImage textureBuffer = ImageIO.read(fileStream);

        final int textureWidth = textureBuffer.getWidth();
        assert textureWidth != 0;
        final int textureHeight = textureBuffer.getHeight();
        assert textureHeight != 0;
        final int textureSideProduct = textureWidth * textureHeight;

        final int[] pixels = new int[textureSideProduct];
        textureBuffer.getRGB(0, 0, textureWidth, textureHeight, pixels, 0, textureWidth);
        ByteBuffer byteBuffer = BufferUtils.createByteBuffer(textureSideProduct * BYTES_PER_PIXEL);

        for (int y = 0; y < textureHeight; y++) {
            for (int x = 0; x < textureWidth; x++) {
                final int pixel = pixels[y * textureWidth + x];
                byteBuffer.put((byte) ((pixel >> 16) & 0xFF)); // Red component
                byteBuffer.put((byte) ((pixel >> 8) & 0xFF)); // Green component
                byteBuffer.put((byte) (pixel & 0xFF)); // Blue component
                byteBuffer.put((byte) ((pixel >> 24) & 0xFF)); // Alpha component
            }
        }

        byteBuffer.flip();

        final int textureName = GL11.glGenTextures();
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, textureName);
        GL12.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL12.GL_CLAMP_TO_EDGE);
        GL12.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL12.GL_CLAMP_TO_EDGE);

        GL12.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST);
        GL12.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);

        GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGBA8, textureWidth,
                textureHeight, 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, byteBuffer);

        assert textureName != 0;
        textureList.add(textureName);

        return textureName;
    }

    /**
     * Stores the provided FloatBuffer into the provided attribute index.
     * @param data The FloatBuffer to store in the GPU's memory.
     * @param attributeIndex The vertex attribute index to store the data in.
     * @param dimensions The number of dimensions the provided FloatBuffer has.
     */
    private void storeDataInAttributeList(final FloatBuffer data, final int attributeIndex, final int dimensions) {
        final int vboName = GL15.glGenBuffers();
        vboList.add(vboName);
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboName);
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, data, GL15.GL_STATIC_DRAW);
        GL20.glVertexAttribPointer(attributeIndex, dimensions, GL11.GL_FLOAT, false, 0, 0);
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, NONE);
    }

    /**
     * Binds the provided IntBuffer as an index buffer.
     * @param indices The IntBuffer containing the indices to bind.
     */
    private void bindIndicesBuffer(final IntBuffer indices) {
        final int vboName = GL15.glGenBuffers();
        vboList.add(vboName);
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, vboName);
        GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, indices, GL15.GL_STATIC_DRAW);
    }

    /**
     * Deletes all memory allocated on the GPU as this is not tracked by the JVM and thus cannot be collected
     * by the garbage collector. One might think this should be done in the destructor, i.e. finalize, but
     * that's deprecated.
     */
    public void deleteGpuAllocations() {
        vaoList.forEach(GL30::glDeleteVertexArrays);
        vboList.forEach(GL15::glDeleteBuffers);
        textureList.forEach(GL11::glDeleteTextures);
    }
}
