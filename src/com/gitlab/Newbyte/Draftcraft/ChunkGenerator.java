/*
 * Copyright (C) Newbyte. All rights reserved.
 *
 * This file is part of Draftcraft, distributed under the zlib licence. For
 * full terms see the included LICENCE file.
 */

package com.gitlab.Newbyte.Draftcraft;

import org.lwjgl.stb.STBPerlin;

/**
 * Class for generating chunks. A chunk is a 3-dimensional array of BlockType.
 */
public class ChunkGenerator {
    /**
     * The number of blocks wide a chunk should be.
     */
    public final static int CHUNK_WIDTH = 64;
    /**
     * The number of blocks tall a chunk should be.
     */
    public final static int CHUNK_HEIGHT = 32;
    /**
     * Noise function requires float, so do this instead of casting later.
     */
    private final static float CHUNK_WIDTH_F = CHUNK_WIDTH;

    /**
     * Generates a single chunk with a random seed. A chunk is a 3-dimensional array of BlockType.
     * @return A randomly generated chunk.
     */
    public BlockType[][][] generateChunk() {
        final BlockType[][][] blocks = new BlockType[CHUNK_WIDTH][CHUNK_WIDTH][CHUNK_HEIGHT];
        final int seed = (int) (System.currentTimeMillis() / 1000);

        // z is elevation
        for (int x = 0; x < blocks.length; x++) {
            for (int y = 0; y < blocks[x].length; y++) {
                final double clamped = org.joml.Math.clamp(-1.0, 1.0, STBPerlin.stb_perlin_noise3_seed(
                        x / CHUNK_WIDTH_F, y / CHUNK_WIDTH_F, 0, 0, 0, 0, seed));

                final int blockHeight = (int) ((clamped + 1) * CHUNK_HEIGHT) / 2;

                final BlockType blockToAdd;

                if (blockHeight > 14) {
                    blockToAdd = BlockType.STONE;
                } else {
                    blockToAdd = BlockType.SAND;
                }

                blocks[x][y][blockHeight] = blockToAdd;
            }
        }

        return blocks;
    }
}
