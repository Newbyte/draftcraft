/*
 * Copyright (C) Newbyte. All rights reserved.
 *
 * This file is part of Draftcraft, distributed under the zlib licence. For
 * full terms see the included LICENCE file.
 */

package com.gitlab.Newbyte.Draftcraft;

/**
 * A model without a texture. Stored in GPU memory as a vertex array object (or VAO for short).
 */
public class BareModel {
    private final int vaoName;
    private final int vertexCount;

    public BareModel(final int vaoName, final int vertexCount) {
        this.vaoName = vaoName;
        this.vertexCount = vertexCount;
    }

    /**
     * Returns the VAO name this model has.
     * @return The VAO name for this model.
     */
    public int getVaoName() {
        assert vaoName != 0;
        return vaoName;
    }

    /**
     * Returns the number of vertices that this model is made up of.
     * @return The number of vertices that make up this model.
     */
    public int getVertexCount() {
        assert vertexCount != 0;
        return vertexCount;
    }
}
