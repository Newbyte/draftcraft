/*
 * Copyright (C) Newbyte. All rights reserved.
 *
 * This file is part of Draftcraft, distributed under the zlib licence. For
 * full terms see the included LICENCE file.
 */

package com.gitlab.Newbyte.Draftcraft;

/**
 * A model with a texture. Texture is stored in GPU memory with a corresponding texture name.
 */
public class TexturedModel extends BareModel {
    private final int textureName;

    public TexturedModel(final int vaoName, final int vertexCount, final int textureName) {
        super(vaoName, vertexCount);
        this.textureName = textureName;
    }

    /**
     * Returns the name of the texture this model has.
     * @return The name of this model's texture.
     */
    public int getTextureName() {
        assert textureName != 0;
        return textureName;
    }
}
