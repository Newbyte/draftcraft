/*
 * Copyright (C) Newbyte. All rights reserved.
 *
 * This file is part of Draftcraft, distributed under the zlib licence. For
 * full terms see the included LICENCE file.
 */

package com.gitlab.Newbyte.Draftcraft;

import org.joml.Math;
import org.joml.Vector3f;

/**
 * An Entity is something that exists in the game world that is not a block, such as the player.
 */
public abstract class Entity implements EntityI {
    /**
     * The acceleration given to the player when they jump.
     */
    private final static float JUMP_ACCELERATION_VALUE = -5;
    /**
     * The value to "nudge" the player by when jumping to prevent the acceleration from being set to 0.
     */
    private final static float JUMP_NUDGE_VALUE = 0.1f;
    /**
     * Increment which is added to the downwards acceleration each tick.
     */
    private final static float ACCELERATION_PER_TICK = 0.1f;
    /**
     * Factor which the acceleration is multiplied by before mutating the Entity's position by it.
     */
    private final static float ACCELERATION_FACTOR = -0.01f;
    protected final ChunkManager chunkManager;
    protected final Vector3f position;
    protected float accelerationZ = 0;

    protected Entity(final Vector3f position, final ChunkManager chunkManager) {
        this.position = position;
        this.chunkManager = chunkManager;
    }

    /**
     * Returns the height of the entity.
     * @return The height of the entity.
     */
    protected abstract int getHeight();

    /**
     * Makes the entity jump.
     */
    protected void jump() {
        if (!isOnGround()) {
            accelerationZ = JUMP_ACCELERATION_VALUE;
            // If we're on the ground our acceleration will be set to 0, so nudge us up slightly to
            // prevent that from happening.
            position.z += JUMP_NUDGE_VALUE;
        }
    }

    /**
     * Returns whether the entity is on the ground.
     *
     * @return True if the entity is on the ground.
     */
    protected boolean isOnGround() {
        final Vector3f groundPosition = new Vector3f(position);

        groundPosition.z -= getHeight();

        return chunkManager.isInsideWorld(groundPosition) && chunkManager.isOutsideBlock(groundPosition);
    }

    public Vector3f getPosition() {
        return position;
    }

    /**
     * Mutates the Z coordinate (elevation) of the entity.
     *
     * @param z The value to mutate the Z coordinate by.
     */
    public void mutateZ(final float z) {
        position.z += z;
    }

    public void onTick() {
        if (isOnGround()) {
            accelerationZ += ACCELERATION_PER_TICK;
            mutateZ(ACCELERATION_FACTOR * accelerationZ);
        } else {
            accelerationZ = 0;
        }
    }

    public void onSpawn() {
        final int x = Math.round(position.x);
        final int y = Math.round(position.y);

        for (int z = 0; z < ChunkGenerator.CHUNK_HEIGHT; z++) {
            if (chunkManager.getBlocks()[x][y][z] != null) {
                position.z = z + getHeight();
            }
        }
    }
}
