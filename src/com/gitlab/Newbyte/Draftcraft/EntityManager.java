/*
 * Copyright (C) Newbyte. All rights reserved.
 *
 * This file is part of Draftcraft, distributed under the zlib licence. For
 * full terms see the included LICENCE file.
 */

package com.gitlab.Newbyte.Draftcraft;

import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;

/**
 * A class that manages all Entities, e.g. ensuring that their onSpawn and onTick methods are called.
 */
public class EntityManager extends TimerTask {
    private final List<EntityI> entities = new ArrayList<>();

    /**
     * Adds an entity to the list of processed entities.
     * @param entity Entity to process.
     */
    public void addEntity(final Entity entity) {
        entities.add(entity);
        entity.onSpawn();
    }

    /**
     * Handle gravity and other things that periodically need to be done with entities.
     */
    public void updateEntities() {
        entities.forEach(EntityI::onTick);
    }

    /**
     * Renders all entities.
     */
    public void renderEntities() {
        entities.forEach(EntityI::render);
    }

    @Override
    public void run() {
        updateEntities();
    }
}
