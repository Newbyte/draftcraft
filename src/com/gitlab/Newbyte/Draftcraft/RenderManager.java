/*
 * Copyright (C) Newbyte. All rights reserved.
 *
 * This file is part of Draftcraft, distributed under the zlib licence. For
 * full terms see the included LICENCE file.
 */

package com.gitlab.Newbyte.Draftcraft;

import org.joml.Matrix4f;
import org.lwjgl.opengl.GL11;

/**
 * Manages rendering the sky and sets up OpenGL state for proper rendering.
 */
public class RenderManager {
    /**
     * The "far plane". Beyond this distance things won't be rendered. Try setting it to a very low number to get the
     * idea. You can also find more information about it and why it exists by searching online.
     */
    public static final float FAR_PLANE = 10000;
    /**
     * The "near plane". Very similar to the far plane except that it determines how close something must be to the
     * camera to not be rendered instead of how far from.
     */
    public static final float NEAR_PLANE = 0.1f;
    /**
     * The camera's field of view. Determines how far to the sides the player can see. Try setting it to a higher or
     * lower value to get the idea.
     */
    public static final float FOV = 70;
    private final static class SkyColour {
        /**
         * The red component of the sky.
         */
        public static final float RED = 0.4f;
        /**
         * The green component of the sky.
         */
        public static final float GREEN = 0.7f;
        /**
         * The blue component of the sky.
         */
        public static final float BLUE = 1.0f;
        /**
         * The alpha component of the sky.
         */
        public static final float ALPHA = 1.0f;
    }

    public RenderManager(final Maths maths, final WindowManager windowManager, final StaticShader shader) {
        final Matrix4f projectionMatrix
                = maths.makeProjectionMatrix(windowManager.getDefaultWidth(), windowManager.getDefaultHeight());
        shader.run();
        shader.loadProjectionMatrix(projectionMatrix);
        shader.stop();
        GL11.glEnable(GL11.GL_DEPTH_TEST);
        GL11.glClearColor(SkyColour.RED, SkyColour.GREEN, SkyColour.BLUE, SkyColour.ALPHA);
    }

    /**
     * Prepares the buffer for rendering a new frame.
     */
    public void reset() {
        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
    }
}
