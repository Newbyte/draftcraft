/*
 * Copyright (C) Newbyte. All rights reserved.
 *
 * This file is part of Draftcraft, distributed under the zlib licence. For
 * full terms see the included LICENCE file.
 */

package com.gitlab.Newbyte.Draftcraft;

/**
 * Pseudo-namespace for constants not associated to any particular class. This is deliberately declared abstract as it
 * should never be instantiated.
 */
public abstract class Constants {
    /**
     * The name the game should use to refer to itself.
     */
    public static final String APP_NAME = "Draftcraft";
    /**
     * Utility constant for use with OpenGL where 0 means no program/object/et cetera.
     */
    public static final int NONE = 0;
    /**
     * Utility constant declaring the size of a Matrix4f from JOML.
     */
    public static final int MATRIX4F_SIZE = 16;
}
