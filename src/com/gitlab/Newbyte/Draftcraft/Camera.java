/*
 * Copyright (C) Newbyte. All rights reserved.
 *
 * This file is part of Draftcraft, distributed under the zlib licence. For
 * full terms see the included LICENCE file.
 */

package com.gitlab.Newbyte.Draftcraft;

import org.joml.Vector3f;

/**
 * Keeps track of the camera's position in the game world.
 */
public class Camera {
    private Vector3f position = null;
    private FirstPersonOrientation orientation = null;

    /**
     * Makes the camera follow the provided Player object.
     * @param player Player to follow.
     */
    public void derive(final Player player) {
        position = player.getPosition();
        orientation = player.getOrientation();
    }

    /**
     * Returns the position of the camera.
     * @return The camera's position.
     */
    public Vector3f getPosition() {
        return position;
    }

    /**
     * Returns the orientation of the camera.
     * @return The camera's orientation.
     */
    public FirstPersonOrientation getOrientation() {
        return orientation;
    }
}
