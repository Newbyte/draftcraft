/*
 * Copyright (C) Newbyte. All rights reserved.
 *
 * This file is part of Draftcraft, distributed under the zlib licence. For
 * full terms see the included LICENCE file.
 */

package com.gitlab.Newbyte.Draftcraft;

import org.joml.Vector3f;

/**
 * Base interface for entities. Useful for making EntityManager-compatible entities that may not fit from the default
 * behaviours provided by the Entity abstract class.
 */
public interface EntityI {
    /**
     * Returns the position of the entity.
     *
     * @return The position of the entity.
     */
    Vector3f getPosition();
    /**
     * Called each tick. Default implementation handles gravity.
     */
    void onTick();
    /**
     * Called when the Entity is added to the EntityManager ("spawned"). Default implementation handles spawn elevation.
     */
    void onSpawn();
    /**
     * Renders the entity.
     */
    void render();
}
