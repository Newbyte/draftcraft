/*
 * Copyright (C) Newbyte. All rights reserved.
 *
 * This file is part of Draftcraft, distributed under the zlib licence. For
 * full terms see the included LICENCE file.
 */

package com.gitlab.Newbyte.Draftcraft;

import org.joml.Vector3f;

import java.util.Set;

import static org.lwjgl.glfw.GLFW.*;

/**
 * Represents the player in the game and manages player movement and world interactions.
 */
public class Player extends Entity implements InputListener {
    private final static byte MOUSE_MOVEMENT_SCALE = 100;
    private final static float MOVEMENT_SPEED = 0.05f;
    private final FirstPersonOrientation orientation = new FirstPersonOrientation();
    private final Set<Integer> pressedKeys;

    protected Player(final Vector3f position, final ChunkManager chunkManager, final Set<Integer> pressedKeys) {
        super(position, chunkManager);
        this.pressedKeys = pressedKeys;
        orientation.pitch = (float) (-Math.PI / 2.0);
    }

    @Override
    public void onTick() {
        super.onTick();

        pressedKeys.forEach(key -> {
            switch (key) {
                case GLFW_KEY_SPACE -> jump();
                case GLFW_KEY_W -> moveForwards();
            }
        });
    }

    /**
     * Makes the player move in the direction it is looking.
     */
    private void moveForwards() {
        final double deltaX = Math.sin(orientation.yaw) * MOVEMENT_SPEED;
        final double deltaY = Math.cos(orientation.yaw) * MOVEMENT_SPEED;
        final int newX = (int) Math.round(position.x + deltaX);
        final int newY = (int) Math.round(position.y + deltaY);

        boolean shouldPass = true;

        for (int i = 0; i < getHeight(); i++) {
            final int newZ = (int) position.z - i;

            final Vector3f newPositionVector = new Vector3f(newX, newY, newZ);
            final boolean insideWorld = chunkManager.isInsideWorld(newPositionVector);

            if (insideWorld && !chunkManager.isOutsideBlock(newPositionVector)) {
                shouldPass = false;
            }
        }

        if (shouldPass) {
            position.x += (float) deltaX;
            position.y += (float) deltaY;
        }
    }

    @Override
    protected int getHeight() {
        return 2;
    }

    @Override
    public void render() {
        // Deliberately empty as the player is never seen
    }

    /**
     * Returns the player's orientation.
     * @return The player's orientation.
     */
    public FirstPersonOrientation getOrientation() {
        return orientation;
    }

    @Override
    public void mouseButtonPressed(int button) {
        if (!chunkManager.isInsideWorld(position)) {
            return;
        }

        final Vector3f ray = new Vector3f(position);
        final Vector3f rayIncrement = new Vector3f();

        while (chunkManager.isInsideWorld(ray) && chunkManager.isOutsideBlock(new Vector3f(ray))) {
            rayIncrement.x = (float) Math.sin(orientation.yaw);
            rayIncrement.y = (float) Math.cos(orientation.yaw);
            rayIncrement.z = (float) -Math.cos(orientation.pitch);

            ray.add(rayIncrement);
        }

        switch (button) {
            case GLFW_MOUSE_BUTTON_1 -> chunkManager.getBlocks()[(int) ray.x][(int) ray.y][(int) ray.z] = null;
            case GLFW_MOUSE_BUTTON_2 -> {
                ray.sub(rayIncrement);
                chunkManager.getBlocks()[(int) ray.x][(int) ray.y][(int) ray.z] = BlockType.STONE;
            }
        }

    }

    @Override
    public void cursorMoved(final double deltaX, final double deltaY) {
        orientation.yaw += (float) (deltaX / MOUSE_MOVEMENT_SCALE);
        orientation.pitch += (float) (deltaY / MOUSE_MOVEMENT_SCALE);

        if (orientation.pitch < -Math.PI) {
            orientation.pitch = (float) -Math.PI;
        } else if (orientation.pitch > 0) {
            orientation.pitch = 0;
        }
    }
}
