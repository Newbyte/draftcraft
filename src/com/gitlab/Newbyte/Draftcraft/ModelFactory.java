/*
 * Copyright (C) Newbyte. All rights reserved.
 *
 * This file is part of Draftcraft, distributed under the zlib licence. For
 * full terms see the included LICENCE file.
 */

package com.gitlab.Newbyte.Draftcraft;

import java.io.IOException;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

/**
 * Factory for creating 3D models. See methods for available models.
 */
public class ModelFactory {
    private final BufferMaker bufferMaker;
    private final Loader loader;

    public ModelFactory(final BufferMaker bufferMaker, final Loader loader) {
        this.bufferMaker = bufferMaker;
        this.loader = loader;
    }

    /**
     * Makes a block model.
     * @return A block model.
     * @throws IOException Thrown if the block texture could not be loaded for some reason.
     */
    public TexturedModel makeBlock(final String texturePath) throws IOException {
        // Oh no! Magic numbers!
        // Short of storing this in a file and using some sort of model loader I don't see any prettier way of doing
        // this. Using constants here won't make anything prettier.
        final float[] vertices = {
                -0.5f,0.5f,-0.5f,
                -0.5f,-0.5f,-0.5f,
                0.5f,-0.5f,-0.5f,
                0.5f,0.5f,-0.5f,

                -0.5f,0.5f,0.5f,
                -0.5f,-0.5f,0.5f,
                0.5f,-0.5f,0.5f,
                0.5f,0.5f,0.5f,

                0.5f,0.5f,-0.5f,
                0.5f,-0.5f,-0.5f,
                0.5f,-0.5f,0.5f,
                0.5f,0.5f,0.5f,

                -0.5f,0.5f,-0.5f,
                -0.5f,-0.5f,-0.5f,
                -0.5f,-0.5f,0.5f,
                -0.5f,0.5f,0.5f,

                -0.5f,0.5f,0.5f,
                -0.5f,0.5f,-0.5f,
                0.5f,0.5f,-0.5f,
                0.5f,0.5f,0.5f,

                -0.5f,-0.5f,0.5f,
                -0.5f,-0.5f,-0.5f,
                0.5f,-0.5f,-0.5f,
                0.5f,-0.5f,0.5f
        };

        final int[] indices = {
                0,1,3,
                3,1,2,
                4,5,7,
                7,5,6,
                8,9,11,
                11,9,10,
                12,13,15,
                15,13,14,
                16,17,19,
                19,17,18,
                20,21,23,
                23,21,22
        };

        final float[] uv = {
                0,0,
                0,1,
                1,1,
                1,0,
                0,0,
                0,1,
                1,1,
                1,0,
                0,0,
                0,1,
                1,1,
                1,0,
                0,0,
                0,1,
                1,1,
                1,0,
                0,0,
                0,1,
                1,1,
                1,0,
                0,0,
                0,1,
                1,1,
                1,0
        };

        final FloatBuffer verticesBuffer = bufferMaker.makeFloatBuffer(vertices);
        final IntBuffer indicesBuffer = bufferMaker.makeIntBuffer(indices);
        final FloatBuffer uvBuffer = bufferMaker.makeFloatBuffer(uv);

        final int vaoName = loader.loadToVao(verticesBuffer, indicesBuffer, uvBuffer);
        final int textureName = loader.loadTexture(texturePath);

        assert vaoName != 0;
        assert textureName != 0;

        return new TexturedModel(vaoName, indices.length, textureName);
    }
}
