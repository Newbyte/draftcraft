/*
 * Copyright (C) Newbyte. All rights reserved.
 *
 * This file is part of Draftcraft, distributed under the zlib licence. For
 * full terms see the included LICENCE file.
 */

package com.gitlab.Newbyte.Draftcraft;

import org.joml.Vector3f;

import java.io.IOException;
import java.util.Timer;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Manages setting up the right state and starting up the game.
 */
public class GameManager {
    private final static Logger LOGGER = Logger.getLogger(GameManager.class.getName());
    private final static int ENTITY_UPDATE_RATE = 10;
    private final static int ENTITY_FIRST_UPDATE_DELAY = 0;

    public static void main(final String[] args) {
        LOGGER.setLevel(Level.ALL);
        FileHandler logFileHandler = null;

        for (final String arg : args) {
            if (arg.equals("--log-to-file")) {
                try {
                    logFileHandler = new FileHandler("draftcraft-log-" + System.currentTimeMillis() + ".txt");
                    LOGGER.addHandler(logFileHandler);
                    final SimpleFormatter simpleFormatter = new SimpleFormatter();
                    logFileHandler.setFormatter(simpleFormatter);
                    logFileHandler.setLevel(Level.ALL);
                } catch (final IOException exception) {
                    LOGGER.log(Level.WARNING, "Failed to create a FileHandler! Logging to file will be disabled.", exception);
                }
            }
        }
        final ChunkGenerator chunkGenerator = new ChunkGenerator();
        final ChunkManager chunkManager = new ChunkManager(chunkGenerator);
        final EntityManager entityManager = new EntityManager();
        final Loader loader = new Loader();
        final WindowManager windowManager;
        try {
             windowManager = new WindowManager(logFileHandler);
        } catch (final IllegalStateException exception) {
            LOGGER.log(Level.SEVERE, "Failed to create a WindowManager!", exception);
            return;
        }
        final Player player = new Player(new Vector3f(0, 5, 1), chunkManager, windowManager.getPressedKeysView());
        entityManager.addEntity(player);
        final Timer entityTimer = new Timer(false);
        entityTimer.scheduleAtFixedRate(entityManager, ENTITY_FIRST_UPDATE_DELAY, ENTITY_UPDATE_RATE);
        final Camera camera = new Camera();
        final BufferMaker bufferMaker = new BufferMaker();
        final ModelFactory modelFactory = new ModelFactory(bufferMaker, loader);
        final Maths maths = new Maths();
        final StaticShader staticShader;
        try {
            staticShader = new StaticShader(logFileHandler);
        } catch (final IOException exception) {
            LOGGER.log(Level.SEVERE, "Failed to create a StaticShader! Unknown IO error. Exiting …", exception);
            return;
        } catch (final IllegalStateException exception) {
            LOGGER.log(
                    Level.SEVERE,
                    "Failed to create a StaticShader! Shader failed to compile (see error above). Exiting …",
                    exception
            );
            return;
        }
        final RenderManager renderManager = new RenderManager(maths, windowManager, staticShader);
        final TexturedModel stoneBlockModel;
        final TexturedModel sandBlockModel;
        try {
             stoneBlockModel = modelFactory.makeBlock("grass.png");
             sandBlockModel = modelFactory.makeBlock("sand.png");
        } catch (final IOException exception) {
            LOGGER.log(Level.SEVERE, "Failed to load block texture! Unknown IO error. Exiting …", exception);
            return;
        }
        final WorldRenderer worldRenderer
                = new WorldRenderer(chunkManager, maths, staticShader, sandBlockModel, stoneBlockModel);
        final JumpingBlock jumpingBlock = new JumpingBlock(
                new Vector3f(6, 7, 1),
                chunkManager,
                stoneBlockModel,
                worldRenderer,
                staticShader
        );
        entityManager.addEntity(jumpingBlock);
        camera.derive(player);
        windowManager.addInputListener(player);
        windowManager.toggleCursorMode();

        // Rendering loop
        while (!windowManager.shouldWindowClose()) {
            renderManager.reset();
            staticShader.run();
            staticShader.loadViewMatrix(maths.makeViewMatrix(camera));
            worldRenderer.renderBlocks();
            entityManager.renderEntities();
            staticShader.stop();
            windowManager.updateDisplay();
        }

        // Resource cleanup
        entityTimer.cancel();
        staticShader.cleanup();
        loader.deleteGpuAllocations();
        windowManager.destroy();
    }
}
