/*
 * Copyright (C) Newbyte. All rights reserved.
 *
 * This file is part of Draftcraft, distributed under the zlib licence. For
 * full terms see the included LICENCE file.
 */

package com.gitlab.Newbyte.Draftcraft;

import org.joml.Matrix4f;

import java.io.IOException;
import java.util.logging.FileHandler;

/**
 * A shader loader for a shader program that renders a model and maps a texture to it.
 */
public class StaticShader extends ShaderProgram {
    private static final String vertexShaderPath = "vertex-shader.vert";
    private static final String fragmentShaderPath = "fragment-shader.frag";

    private int transformationMatrixLocation;
    private int projectionMatrixLocation;
    private int viewMatrixLocation;

    public StaticShader(final FileHandler logFileHandler) throws IOException {
        super(vertexShaderPath, fragmentShaderPath, logFileHandler);
    }

    /**
     * Load a transformation matrix into this shader program.
     * @param matrix The transformation matrix to load.
     */
    public void loadTransformationMatrix(final Matrix4f matrix) {
        super.loadMatrix(transformationMatrixLocation, matrix);
    }

    /**
     * Load a projection matrix into this shader program.
     * @param matrix The projection matrix to load.
     */
    public void loadProjectionMatrix(final Matrix4f matrix) {
        super.loadMatrix(projectionMatrixLocation, matrix);
    }

    /**
     * Load a view matrix into this shader program.
     * @param matrix The view matrix to load.
     */
    public void loadViewMatrix(final Matrix4f matrix) {
        super.loadMatrix(viewMatrixLocation, matrix);
    }

    @Override
    protected void bindAttributes() {
        super.bindAttribute(0, "position");
        super.bindAttribute(1, "textureCoords");
    }

    @Override
    protected void getAllUniformLocations() {
        transformationMatrixLocation = super.getUniformLocation("transformationMatrix");
        projectionMatrixLocation = super.getUniformLocation("projectionMatrix");
        viewMatrixLocation = super.getUniformLocation("viewMatrix");
    }
}
