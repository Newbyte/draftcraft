/*
 * Copyright (C) Newbyte. All rights reserved.
 *
 * This file is part of Draftcraft, distributed under the zlib licence. For
 * full terms see the included LICENCE file.
 */

package com.gitlab.Newbyte.Draftcraft;

import org.joml.Vector3f;

import java.util.ArrayList;
import java.util.List;

/**
 * Manages the chunks in the world. A chunk is a 3-dimensional array of BlockType.
 */
public class ChunkManager {
    private final List<List<BlockType[][][]>> chunks = new ArrayList<>();

    public ChunkManager(final ChunkGenerator chunkGenerator) {
        chunks.add(new ArrayList<>());
        chunks.get(0).add(chunkGenerator.generateChunk());
    }

    /**
     * Utility method for checking whether a given position is inside a block.
     * @param position The position to check. Note that this variable is mutated in this method, so clone if necessary.
     * @return True if the provided position is inside a block.
     */
    public boolean isOutsideBlock(final Vector3f position) {
        position.round();

        return getBlocks()[(int) position.x][(int) position.y][(int) position.z] == null;
    }

    /**
     * Utility method for checking whether a given position is inside the game world.
     * @param position The position to check.
     * @return True if the provided position is inside the game world.
     */
    public boolean isInsideWorld(final Vector3f position) {
        return position.x <= ChunkGenerator.CHUNK_WIDTH - 1
                && position.x > 0
                && position.y <= ChunkGenerator.CHUNK_WIDTH - 1
                && position.y > 0
                && position.z <= ChunkGenerator.CHUNK_HEIGHT - 1
                && position.z > 0;
    }

    /**
     * Returns all the blocks in the world.
     * @return All the blocks in the world.
     */
    public BlockType[][][] getBlocks() {
        return chunks.get(0).get(0);
    }
}
