/*
 * Copyright (C) Newbyte. All rights reserved.
 *
 * This file is part of Draftcraft, distributed under the zlib licence. For
 * full terms see the included LICENCE file.
 */

package com.gitlab.Newbyte.Draftcraft;

/**
 * Used for declaring which type of block a certain block should be. A null BlockType means it's an air block.
 */
public enum BlockType {
    STONE, SAND
}
