/*
 * Copyright (C) Newbyte. All rights reserved.
 *
 * This file is part of Draftcraft, distributed under the zlib licence. For
 * full terms see the included LICENCE file.
 */

package com.gitlab.Newbyte.Draftcraft;

import org.lwjgl.BufferUtils;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

/**
 * Contains utility methods for creating NIO buffers and prepares them for use with OpenGL.
 */
public class BufferMaker {
    /**
     * Utility method to facilitate the reaction of an IntBuffer.
     * @param data Data to put in the IntBuffer.
     * @return An IntBuffer containing the given data.
     */
    public IntBuffer makeIntBuffer(final int[] data) {
        final IntBuffer buffer = BufferUtils.createIntBuffer(data.length);
        buffer.put(data);
        buffer.flip();

        return buffer;
    }

    /**
     * Utility method to facilitate the reaction of a FloatBuffer.
     * @param data Data to put in the FloatBuffer.
     * @return A FloatBuffer containing the given data.
     */
    public FloatBuffer makeFloatBuffer(final float[] data) {
        final FloatBuffer buffer = BufferUtils.createFloatBuffer(data.length);
        buffer.put(data);
        buffer.flip();

        return buffer;
    }
}
