/*
 * Copyright (C) Newbyte. All rights reserved.
 *
 * This file is part of Draftcraft, distributed under the zlib licence. For
 * full terms see the included LICENCE file.
 */

package com.gitlab.Newbyte.Draftcraft;

import org.joml.Matrix4f;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;

import java.io.IOException;
import java.io.InputStream;
import java.nio.FloatBuffer;
import java.nio.charset.StandardCharsets;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.gitlab.Newbyte.Draftcraft.Constants.MATRIX4F_SIZE;
import static com.gitlab.Newbyte.Draftcraft.Constants.NONE;

/**
 * A class for managing a shader program.
 */
public abstract class ShaderProgram {
    private final static String SHADER_PATH = "shaders/";
    private final Logger logger = Logger.getLogger(ShaderProgram.class.getName());
    private final int programObject;
    private final int vertexShaderObject;
    private final int fragmentShaderObject;

    public ShaderProgram(
            final String vertexShaderPath,
            final String fragmentShaderPath,
            final FileHandler logFileHandler
    ) throws IllegalStateException, IOException {
        if (logFileHandler != null) {
            logger.addHandler(logFileHandler);
        }
        programObject = GL20.glCreateProgram();
        vertexShaderObject = loadShader(vertexShaderPath, GL20.GL_VERTEX_SHADER);
        fragmentShaderObject = loadShader(fragmentShaderPath, GL20.GL_FRAGMENT_SHADER);

        GL20.glAttachShader(programObject, vertexShaderObject);
        GL20.glAttachShader(programObject, fragmentShaderObject);
        bindAttributes();
        GL20.glLinkProgram(programObject);
        GL20.glValidateProgram(programObject);
        getAllUniformLocations();
    }

    /**
     * Bind all attributes the shader program has.
     */
    protected abstract void bindAttributes();

    /**
     * Populates all uniform variables.
     */
    protected abstract void getAllUniformLocations();

    /**
     * Runs the shader program on the GPU.
     */
    public void run() {
        GL20.glUseProgram(programObject);
    }

    /**
     * Sets OpenGL to not use any shader program.
     */
    public void stop() {
        GL20.glUseProgram(NONE);
    }

    /**
     * Cleans up state not tracked by the JVM, i.e. delete shaders and program.
     * Also tells OpenGL to not use any program.
     */
    public void cleanup() {
        stop();
        GL20.glDetachShader(programObject, vertexShaderObject);
        GL20.glDetachShader(programObject, fragmentShaderObject);
        GL20.glDeleteShader(vertexShaderObject);
        GL20.glDeleteShader(fragmentShaderObject);
        GL20.glDeleteProgram(programObject);
    }

    /**
     * Binds an attribute to the current shader program.
     * @param attributeIndex The index to bind the variable to.
     * @param variableName The name of the variable.
     */
    protected void bindAttribute(final int attributeIndex, final String variableName) {
        GL20.glBindAttribLocation(programObject, attributeIndex, variableName);
    }

    /**
     * Loads a matrix into the shader.
     * @param location Location to load the matrix into.
     * @param matrix The matrix to load into the shader.
     */
    protected void loadMatrix(final int location, final Matrix4f matrix) {
        final FloatBuffer matrixBuffer = BufferUtils.createFloatBuffer(MATRIX4F_SIZE);
        matrix.get(matrixBuffer);
        GL20.glUniformMatrix4fv(location, false, matrixBuffer);
    }

    /**
     * Returns the location of a uniform variable.
     * @param variableName The variable to return the location of.
     * @return The variable's location.
     */
    protected int getUniformLocation(final String variableName) {
        return GL20.glGetUniformLocation(programObject, variableName);
    }

    /**
     * Loads a shader from disk.
     * @param pathString The path to the shader. Root is resource root + "shaders/
     * @param type The type of shader contained in the file pointed to in the path.
     *             Use GL20.GL_VERTEX_SHADER or GL20.GL_FRAGMENT_SHADER.
     * @return The shader object the newly loaded shader was assigned.
     * @throws IllegalStateException Thrown if the shader fails to compile.
     * @throws IOException Thrown if the shader fails to load from disk.
     */
    private int loadShader(final String pathString, final int type) throws IllegalStateException, IOException {
        final InputStream inputStream = ClassLoader.getSystemResourceAsStream(SHADER_PATH + pathString);
        assert inputStream != null;
        final String shaderSource = new String(inputStream.readAllBytes(), StandardCharsets.UTF_8);

        final int shaderObject = GL20.glCreateShader(type);
        GL20.glShaderSource(shaderObject, shaderSource);
        GL20.glCompileShader(shaderObject);

        if (GL20.glGetShaderi(shaderObject, GL20.GL_COMPILE_STATUS) == GL11.GL_FALSE) {
            logger.log(Level.SEVERE, "Failed to compile shader: " + pathString);
            logger.log(Level.SEVERE, GL20.glGetShaderInfoLog(shaderObject));
            throw new IllegalStateException();
        }

        // 0 means no shader, so this should never happen
        assert shaderObject != 0;

        return shaderObject;
    }
}
