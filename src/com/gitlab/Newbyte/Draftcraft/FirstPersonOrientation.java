/*
 * Copyright (C) Newbyte. All rights reserved.
 *
 * This file is part of Draftcraft, distributed under the zlib licence. For
 * full terms see the included LICENCE file.
 */

package com.gitlab.Newbyte.Draftcraft;

/**
 * Utility class for managing a first-person perspective orientation. Has two fields for Euler angles in radians.
 * There's no point in using getters and setters here as this class by design does not execute any code itself and using
 * such accessors would only make usage needlessly awkward.
 */
public class FirstPersonOrientation {
    /**
     * The Euler angle pitch of the first-person orientation.
     */
    public float pitch = 0;
    /**
     * The Euler angle yaw of the first-person orientation.
     */
    public float yaw = 0;
}
